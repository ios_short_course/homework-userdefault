//
//  SuccessViewController.swift
//  Homework_UserDefault
//
//  Created by Sarath LUN on 12/10/18.
//  Copyright © 2018 Sarath LUN. All rights reserved.
//

import UIKit

class SuccessViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func onLogoutTouchUpInside(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "isLogin")
        // load login screen
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let loginViewController = storyBoard.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
        self.present(loginViewController, animated:true, completion:nil)
        
    }
    
}
