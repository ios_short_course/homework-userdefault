//
//  SignupViewController.swift
//  Homework_UserDefault
//
//  Created by Sarath LUN on 12/9/18.
//  Copyright © 2018 Sarath LUN. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {

    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onClickSignUp(_ sender: Any) {
        if userNameTextField.text != "" && userNameTextField.text != nil{
            UserDefaults.standard.set(userNameTextField.text, forKey: "username")
        }else{
            showAlert("Error","Please input Username")
        }
        if emailTextField.text != nil && emailTextField.text != "" {
            UserDefaults.standard.set(emailTextField.text, forKey: "email")
        }else{
            showAlert("Error","Please input Email")
        }
        
        if passwordTextField.text != nil && passwordTextField.text != "" {
            if confirmPasswordTextField.text == passwordTextField.text {
                UserDefaults.standard.set(passwordTextField.text, forKey: "password")
                showAlert("Successful", "Your account has been created.")
            }else{
                showAlert("Error","Confirm password not match")
            }
        }else{
            showAlert("Error","Please input password")
        }
        
        
        
    }
    
    func showAlert(_ title:String, _ msg:String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }

}
