//
//  LoginViewController.swift
//  Homework_UserDefault
//
//  Created by Sarath LUN on 12/9/18.
//  Copyright © 2018 Sarath LUN. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func onLogin(_ sender: Any) {
        if usernameTextField.text == "" || usernameTextField.text == nil {
            showAlert("Error","Please input Username")
        }else if passwordTextField.text == "" || passwordTextField.text == nil{
            showAlert("Error","Please input Password")
        }else{
            // check user exist
            if usernameTextField.text == UserDefaults.standard.string(forKey: "username"){
                // user existed -> check password
                if passwordTextField.text == UserDefaults.standard.string(forKey: "password"){
                    // set isLogin = true
                    UserDefaults.standard.set(true, forKey: "isLogin")
                    print(UserDefaults.standard.bool(forKey: "isLogin"))
                    loadSuccessScreen()
                }else{
                    showAlert("Error","Incorrect Password")
                }
            }else{
                // user not existed
                showAlert("Error","User doesn't exist")
            }
        }
    }
    
    func loadSuccessScreen() {
        // load another view
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let resultViewController = storyBoard.instantiateViewController(withIdentifier: "successViewController") as! SuccessViewController
        self.present(resultViewController, animated:true, completion:nil)
    }
    
    func showAlert(_ title:String,_ msg:String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }

}
